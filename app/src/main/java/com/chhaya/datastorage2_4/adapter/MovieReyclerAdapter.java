package com.chhaya.datastorage2_4.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.chhaya.datastorage2_4.R;
import com.chhaya.datastorage2_4.config.RoomDatabaseConfig;
import com.chhaya.datastorage2_4.entity.Movie;
import com.chhaya.datastorage2_4.ui.AddMovieActivity;

import java.util.ArrayList;
import java.util.List;

public class MovieReyclerAdapter extends RecyclerView.Adapter<MovieReyclerAdapter.MovieViewHolder> {

    private List<Movie> movieDataSet;
    private Context context;

    public MovieReyclerAdapter(List<Movie> movieDataSet, Context context) {
        this.movieDataSet = movieDataSet;
        this.context = context;
    }

    @NonNull
    @Override
    public MovieReyclerAdapter.MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_item_layout, parent, false);
        return new MovieViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieReyclerAdapter.MovieViewHolder holder, int position) {
        holder.textTitle.setText(movieDataSet.get(position).getTitle());
        holder.textDesc.setText(movieDataSet.get(position).getDescription());
        //Glide.with(context).load(movieDataSet.get(position).getThumbnail()).into(holder.imageMovie);
        holder.imageMovie.setImageURI(movieDataSet.get(position).getThumbnail());


    }

    @Override
    public int getItemCount() {
        return movieDataSet.size();
    }

    public class MovieViewHolder extends RecyclerView.ViewHolder {
        ImageView imageMovie, imageMore;
        TextView textTitle, textDesc;

        public MovieViewHolder(@NonNull View itemView) {
            super(itemView);
            imageMovie = itemView.findViewById(R.id.image_movie);
            textTitle = itemView.findViewById(R.id.text_title);
            textDesc = itemView.findViewById(R.id.text_description);
            imageMore = itemView.findViewById(R.id.image_more);
            imageMore.setOnClickListener(v -> {
                PopupMenu menu = new PopupMenu(context, imageMore);
                menu.getMenuInflater().inflate(R.menu.option_menu, menu.getMenu());
                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.item_delete: {
                                Log.e("TAG", "Delete");
                                Log.e("TAG", movieDataSet.get(getAdapterPosition()).toString());
                                RoomDatabaseConfig.getRoomDb(context).movieDao().deleteOne(movieDataSet.get(getAdapterPosition()));
                                Toast.makeText(context, "Delete successfully", Toast.LENGTH_LONG).show();
                            }
                            case R.id.item_edit: {
                                Log.e("TAG", "Edit");
                                Movie movie = new Movie(
                                        movieDataSet.get(getAdapterPosition()).getTitle(),
                                        movieDataSet.get(getAdapterPosition()).getDescription(),
                                        null
                                );
                                movie.setId(movieDataSet.get(getAdapterPosition()).getId());
                                Intent updateIntent = new Intent(context, AddMovieActivity.class);
                                updateIntent.putExtra("update", true);
                                updateIntent.putExtra("editMovie", movie);
                                context.startActivity(updateIntent);
                                //RoomDatabaseConfig.getRoomDb(context).movieDao().updateOne(movieDataSet.get(position));
                                //Toast.makeText(context, "Update successfully", Toast.LENGTH_LONG).show();
                            }
                        }
                        return true;
                    }
                });
                menu.show();
            });
        }

    }

}
