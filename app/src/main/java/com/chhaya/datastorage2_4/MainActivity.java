package com.chhaya.datastorage2_4;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity {

    private TextView textData;
    private Button btnGet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*SharedPreferences prefs = getPreferences(MODE_PRIVATE);

        SharedPreferences.Editor editor = prefs.edit();

        // Manipulate (add ...)
        //editor.putString("secret", "");

        //editor.apply();

        textData = findViewById(R.id.text_data);
        btnGet = findViewById(R.id.button_get);

        String secret = prefs.getString("secret", "No data");
        textData.setText(secret);

        btnGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writeFile();
            }
        });

        readFile();*/
        String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (!hasPermission(this, permissions)) {
            ActivityCompat.requestPermissions(this, permissions, 112);
            Log.e("TAG", "Request permission");
        } else {
            Log.e("TAG", "Have Permission");
            File file = new File("/sdcard/FileAccess" + "message.txt");
            Log.e("TAG", "Start write");
            String message = "Test with me";
            try {
                FileOutputStream outputStream = new FileOutputStream(file);
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
                Log.e("TAG", "Writing...");
                outputStreamWriter.write(message);
                outputStreamWriter.close();
                Log.e("TAG", "Succeed");
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private void writeFile() {
        String message = "I love you so much";
        String fileName = "text.txt";
        try {
            OutputStreamWriter outputStream = new OutputStreamWriter(openFileOutput(fileName, Context.MODE_APPEND));
            outputStream.write(message);
            outputStream.close();
            Toast.makeText(MainActivity.this, "Write succeed", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readFile() {
        try {
            FileInputStream fileInput = openFileInput("text.txt");
            InputStreamReader inputStream = new InputStreamReader(fileInput);
            BufferedReader bufferedReader = new BufferedReader(inputStream);
            String message = bufferedReader.readLine();
            Log.d("TAG", message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static boolean hasPermission(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String p : permissions) {
                if (ActivityCompat.checkSelfPermission(context, p) != PackageManager.PERMISSION_GRANTED) {
                    Log.e("TAG", "Checking");
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 112: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("TAG", "Allowed...");
                } else {
                    Log.e("TAG", "Not allowed...");
                }
            }
        }
    }
}
