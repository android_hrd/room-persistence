package com.chhaya.datastorage2_4.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.chhaya.datastorage2_4.entity.Movie;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface MovieDao {

    @Insert
    void insert(Movie... movie);

    @Query("SELECT * FROM movie")
    List<Movie> selectAll();

    @Query("SELECT * FROM movie WHERE id = :id")
    Movie selectOne(int id);

    @Delete
    void deleteOne(Movie movie);

    @Update
    void updateOne(Movie movie);

}
