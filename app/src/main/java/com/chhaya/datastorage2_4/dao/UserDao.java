package com.chhaya.datastorage2_4.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.chhaya.datastorage2_4.entity.User;

import java.util.List;

@Dao
public interface UserDao {
    @Query("select * from tbl_user")
    List<User> findAll();

    @Insert
    void insert(User... users);

    @Update
    void update(User user);

    @Delete
    void delete(User user);
}
