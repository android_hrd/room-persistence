package com.chhaya.datastorage2_4.config;

import android.net.Uri;

import androidx.room.TypeConverter;

public class UriConverter {
    @TypeConverter
    public Uri fromString(String value) {
        return value == null? null : Uri.parse(value);
    }

    @TypeConverter

    public String toString(Uri uri){
        return uri.toString();
    }
}
