package com.chhaya.datastorage2_4.config;

import android.content.Context;
import android.net.Uri;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.chhaya.datastorage2_4.dao.MovieDao;
import com.chhaya.datastorage2_4.dao.UserDao;
import com.chhaya.datastorage2_4.entity.Movie;
import com.chhaya.datastorage2_4.entity.User;

@Database(entities = {Movie.class, User.class}, version = 1)
@TypeConverters({UriConverter.class})
public abstract class RoomDatabaseConfig extends RoomDatabase {

    // Create dao methods
    public abstract MovieDao movieDao();
    public abstract UserDao userDao();

    // Create singleton object
    public static RoomDatabaseConfig INSTANCE;

    // Initialize singleton object
    public static RoomDatabaseConfig getRoomDb(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                    RoomDatabaseConfig.class,
                    "db_movie").allowMainThreadQueries().build();
        }
        return INSTANCE;
    }

    // For destroy singleton object
    public static void destroy() {
        INSTANCE = null;
    }


}
