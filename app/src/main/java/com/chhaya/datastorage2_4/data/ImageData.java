package com.chhaya.datastorage2_4.data;

public class ImageData {
    public static final String AVENGERS = "https://cdn.cnn.com/cnnnext/dam/assets/190403144228-avengers-endgame-thumb-imax-poster-exlarge-169.jpg";
    public static final String SPIDER_MAN_FAR_FROM_HOME = "https://insidethemagic-119e2.kxcdn.com/wp-content/uploads/2019/07/Spider-man-800x400.jpg";
}
