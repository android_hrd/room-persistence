package com.chhaya.datastorage2_4.ui;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.chhaya.datastorage2_4.R;
import com.chhaya.datastorage2_4.config.RoomDatabaseConfig;
import com.chhaya.datastorage2_4.entity.Movie;

public class AddMovieActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnChooseImage;
    private Button btnAdd;
    private EditText editTitle;
    private EditText editDesc;
    private ImageView imagePreview;
    private Uri imageUri;
    private int updateId;

    private final static int CHOOSE_IMAGE_REQUEST_CODE = 1;

    private void initViews() {
        btnChooseImage = findViewById(R.id.button_choose_image);
        btnAdd = findViewById(R.id.button_add);
        editTitle = findViewById(R.id.edit_title);
        editDesc = findViewById(R.id.edit_desc);
        imagePreview = findViewById(R.id.image_preview);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_movie);
        initViews();

        btnChooseImage.setOnClickListener(this);
        btnAdd.setOnClickListener(this);
        onNewIntent(getIntent());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_choose_image: {
                // Choose image from gallery
                Intent imageIntent = new Intent(Intent.ACTION_PICK);
                imageIntent.setType("image/*");
                startActivityForResult(imageIntent, CHOOSE_IMAGE_REQUEST_CODE);
            } break;
            case R.id.button_add: {
                if (btnAdd.getText() == "Update") {
                    String title = editTitle.getText().toString();
                    String desc = editDesc.getText().toString();
                    Uri thumbnail = imageUri;
                    Movie movie = new Movie(title, desc, thumbnail);
                    movie.setId(updateId);
                    RoomDatabaseConfig.getRoomDb(this).movieDao().updateOne(movie);
                    Log.e("TAG", movie.toString());
                    Log.i("TAG", "Update movie successfully");
                    finish();
                } else {
                    Log.e("TAG", "Add movie");
                    String title = editTitle.getText().toString();
                    String desc = editDesc.getText().toString();
                    Uri thumbnail = imageUri;
                    Movie movie = new Movie(title, desc, thumbnail);
                    RoomDatabaseConfig.getRoomDb(this).movieDao().insert(movie);
                    Log.e("TAG", movie.toString());
                    Log.i("TAG", "Insert movie successfully");
                    finish();
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CHOOSE_IMAGE_REQUEST_CODE
                && resultCode == RESULT_OK
                && data != null) {
            imageUri = data.getData();
            imagePreview.setImageURI(imageUri);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Bundle extras = intent.getExtras();
        if (extras != null) {
            if (extras.getBoolean("update")) {
                Movie movie = (Movie)extras.getSerializable("editMovie");
                updateId = movie.getId();
                btnAdd.setText("Update");
                editTitle.setText(movie.getTitle());
                editDesc.setText(movie.getDescription());
            }
        }
    }
}
