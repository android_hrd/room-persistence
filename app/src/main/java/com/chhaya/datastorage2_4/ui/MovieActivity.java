package com.chhaya.datastorage2_4.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;

import com.chhaya.datastorage2_4.R;
import com.chhaya.datastorage2_4.adapter.MovieReyclerAdapter;
import com.chhaya.datastorage2_4.config.RoomDatabaseConfig;
import com.chhaya.datastorage2_4.data.ImageData;
import com.chhaya.datastorage2_4.entity.Movie;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class MovieActivity extends AppCompatActivity {

    private final static int READ_INTERNAL_CODE = 100;

    private List<Movie> movieDataSet;
    private MovieReyclerAdapter movieAdapter;
    private LinearLayoutManager layoutManager;
    private RecyclerView rcvMovie;
    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);

        rcvMovie = findViewById(R.id.rcv_movie);




        fab = findViewById(R.id.fab);

        fab.setOnClickListener(v -> {
            Intent addIntent = new Intent(this, AddMovieActivity.class);
            startActivity(addIntent);
        });

        getMovieData();

        if (!checkPermission()) {
            requestPermission();
        }


    }


    private void getMovieData() {
        // Get database here...
        movieDataSet = new ArrayList<>();
        movieDataSet = RoomDatabaseConfig.getRoomDb(this).movieDao().selectAll();
        Log.e("TAG", movieDataSet.toString());
        layoutManager = new LinearLayoutManager(this);
        movieAdapter = new MovieReyclerAdapter(movieDataSet, this);
        rcvMovie.setLayoutManager(layoutManager);
        rcvMovie.setAdapter(movieAdapter);
        movieAdapter.notifyDataSetChanged();
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                READ_INTERNAL_CODE);
    }

    private boolean checkPermission() {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        movieDataSet = null;
        getMovieData();
    }
}
